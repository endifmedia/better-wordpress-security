# Description #

Brings your WordPress site one step closer to a secure fortress. These are must have additions for ALL of your WordPress based sites.

Keep in mind, the below entry may prevent WordPress from running any schedulers. This includes plugins like Backup Buddy. I noticed a lot of traffic to my server was requesting wp-cron.php, so I removed access to it. Feel free to delete this block if you see fit.

    # Prevent access to WP CRON
    <Files wp-cron.php>
     Order Allow,Deny
     Deny from all
    </Files>

Also, you don't need both of the below entries from the wp-config.php file. One will do.

    /**
     * Turn OFF file editing (Recommended)
     * 
     * You can actually stop any user from having access to edit the files in the 
     * WordPress admin area, which means the only way to change the files would be 
     * to upload the files through FTP.
     */
     define( 'DISALLOW_FILE_EDIT', true);

    /**
     * The whole she-bang.. (Adds extra security, but removes the ability to install/update)
     * 
     * You can actually stop any user from having access to edit the files, upload themes,
     * install plugins, etc.,  from in the WordPress admin area. Take THAT hackers!
     */
     define( 'DISALLOW_FILE_MODS', true);

### What is this repository for? ###

* Better WordPress security through .htaccess and wp-config.php.
* Added site speed through compression and expires caching.

### How do I get set up? ###

* Add the items from .htaccess to your root folder's .htaccess file. If you already have a declaration in your .htaccess file from WordPress (inside of # Begin WordPress # End WordPress tags) you can remove the top section from the repo's .htaccess file that contains the same code.

* Drop the /uploads/.htaccess file into your /uploads file.

* Lastly, add the items from the wp-config.php file to your own (refer to the description for each).