<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/**
 * Set post revisions to 3
 * 
 * You can set this number to be whatever you like. 3 is a nice wholesome 
 * number don't you think?
 */
define( 'WP_POST_REVISIONS', 3 );

/**
 * Turn OFF file editing
 * 
 * You can actually stop any user from having access to edit the files in the 
 * WordPress admin area, which means the only way to change the files would be 
 * to upload the files through FTP.
 */
define( 'DISALLOW_FILE_EDIT', true);

/**
 * The whole she-bang..
 * 
 * You can actually stop any user from having access to edit the files, upload themes,
 * install plugins, etc.,  from in the WordPress admin area. Take THAT hackers!
 */
define( 'DISALLOW_FILE_MODS', true);

/**
 * Set autosave interval to 10 mins
 * 
 * You can actually stop any user from having access to edit the files, upload themes,
 * install plugins, etc.,  from in the WordPress admin area. Take THAT hackers!
 */
define('AUTOSAVE_INTERVAL', 600 ); // seconds

/**
 * Disable Autosave 
 * 
 * You can actually stop any user from having access to edit the files, upload themes,
 * install plugins, etc.,  from in the WordPress admin area. Take THAT hackers!
 */
define('WP_POST_REVISIONS', false );